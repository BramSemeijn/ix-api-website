---
title: "Benefits"
date: 2021-04-19T18:38:11+02:00
parent: "/how-it-works"
---

With IX-API, you are able to easily configure, change and cancel IX
services at multiple IXs. The benefits are manifold, both for Internet
Exchanges offering the API and receiving customer requests as well as
for IX customers configuring and changing their IX services.

## Benefits for IX customers 

With IX-API, we will overcome the manual,
error-prone and time-consuming provisioning of services.

For IX customers, IX-API offers the following benefits:

* Configuring or changing IX services can be done without human
  interaction
* It vastly reduces the time for configuring, changing and
  cancelling services from days to minutes 
* It is available 24/7/365 
* Less effort per transaction leads to more cost-efficiency
* Increased transparency due to instant feedback 
* Possibility to integrate IX service in your portals
* Reduced implementation costs by offering one single API language
  for multiple Internet Exchanges 

## Benefits for other IXs 

IX-API not only offers benefits for IX customers but also for IXs
as implementing it drives overall digitalization within the IX:

* Reduced cost of sales 
* More competitive ways to offer low margin services 
* Reduced manual transactions
