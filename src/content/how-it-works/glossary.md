---
title: Glossary
parent: /how-it-works
---

IX-API works through the processing of entities, which we have attempted to
design and name as clearly as possible.

However, there will be some differences in terminology between different groups
of users, so the explanations below clarify how terms are used within the scope
of the IX-API schema and documentation.

The definitions are provided in the order of easiest readability; some
cross-referencing is inevitable.

### Customer
  
A company that buys services from an IXP (and may resell them to other customers).

### Contact

An entity of a company fulfilling a given role and undertaking a specific
responsibility within or on behalf of a customer. This will typically a
department or agent of the customer company rather than an individual, except
in the case of smaller customers.

Several subtypes of contact exist, and a customer may not need to supply every
type. However, ordering certain services may require the customer to specify
contacts of multiple types:

* **Legal**
  > The business entity within the customer that is responsible for the legal
  > contract with the IXP.
* **Billing**
  > The recipient of invoices.
* **Implementation**
  > The contact responsible for provisioning, setting up the BGP testing,
  > routing, etc.
* **NOC**
  > The contact responsible for troubleshooting an ongoing, live service.
* **Peering**
  > The contact responsible for co-ordinating peering with other customers.

### Product

A network or peering-related product of a defined type sold by an IXP to its
customers.

Products will have a defined Product Type; the only type so far defined is
‘Exchange LAN’, for the provision of peering services.

A example of an ‘Exchange LAN’ Product would be each distinct peering
infrastructure available from an IXP.

### Network Service

An instance of a Product, accessible by one or multiple users, depending on the
type of product

In the case of Exchange LAN products, each Network Service is accessed by all
users of the related product, meaning there is a one to one mapping between
products of this type and their related services.

Future Product types may support multiple closed or private instances within a
Product, meaning that there will be one Network Service for each such instance
of a Product.

### Network Feature

A piece of functionality made available to customers within a Network Service.
Any Network Service may offer multiple Network Features of a given type.

Certain features may need to be configured by a customer to use each service.

The only type of Network Feature currently specified is a Route Server, which
can be defined as operating in either Public or Collector mode.

### Network Service Config

A customer’s configuration for usage of a Network Service, eg (for an Exchange
LAN service) the configuration of (a subset of) a Connection for that
customer’s traffic.

### Network Feature Config

A customer’s configuration for usage of a Network Feature, eg (for a Route
Server feature), configuration of a Route Server session for a given Network
Service Config.

### Connection

A functional group of physical network connections collected together into a
LAG (aka Trunk).

### Demarcation Point (aka Demarc)

The point at which customer and IXP networks meet, eg a physical port / socket,
generally with a specified bandwidth.

### Facility

A data centre, with a determined physical address, from which a defined set of
PoPs can be accessed.

### Metro Area

A city-level region used to determine the location of facilities and products.

### PoP

A location within a Facility connected to a single Network Infrastructure, with
defined reachability of other facilities. A single room may contain multiple
PoPs, each linking to a different infrastructure.

### Device

A network hardware device, typically a Switch, located at a specified facility,
connected to one or more PoPs

### MAC address

A MAC address. Addresses may have a defined validity period to support
migrations or changeovers.

### IP address

A IPv4 or IPv6 address, usually supplied by the IXP.

As with MACs addresses, IP addresses may have a defined validity period to
support migrations or changeovers.

### External Reference

A free-text reference used by a customer for their own purposes, typically to
label or locate an entity.

External References are under customer control and have no function effect on
the entities that they reference.

### Contract Reference

A value representing the contract covering a particular entity, supporting the
case where the IXP and customer may have multiple contracts covering different
services or configurations.


