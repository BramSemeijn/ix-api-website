---
title: "How It Works"
date: 2021-04-19T18:32:24+02:00
contentimage: "images/stock1.jpg"
---


## Explore the specs and get the sandbox
The best way to check out how you can use the IX-API is to explore and test the API yourself.

Our “Getting started” section shows you the IX-API specs and how to get the sandbox. Or you jump directly to the API Documentation.

{{< linkBtn href="/how-it-works/getting-started" >}}Getting Started{{< /linkBtn >}}

{{< linkBtn href="https://docs.ix-api.net" >}}Api Documentation{{< /linkBtn >}}

## Why you should test it

With IX-API, you are able to easily configure, change and cancel 
IX services at multiple IXs. The benefits are manifold, both for
Internet Exchanges offering the API and receiving customer
requests as well as for IX customers configuring and changing their
IX services. Read more about the benefits of using IX-API to simplify your IX services.

{{< linkBtn href="/how-it-works/benefits" >}}Benefits{{< /linkBtn >}}

## Questions?
If you have any questions, please do not hesitate to contact us.

