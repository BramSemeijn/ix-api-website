---
title: "API Documentation"
contentimage: images/stock5.jpg
---

You can find more information on how to implement
or integrate IX-API by following these links:

## API V2

* [API Sandbox](https://gitlab.com/ix-api/ix-api-sandbox-v2)
* [OpenAPI Specification](https://docs.ix-api.net/v2/redoc)
* [OpenAPI Specs Download JSON](https://docs.ix-api.net/v2/ix-api-latest.json)
* [OpenAPI Specs Download YAML](https://docs.ix-api.net/v2/ix-api-latest.yml)
* [RFC7807 Problem Details](https://docs.ix-api.net/v2/problems/)

## API V1

* [API Sandbox](https://gitlab.com/ix-api/ix-api-sandbox-v1)
* [OpenAPI Specification](https://docs.ix-api.net/v1/redoc)
* [OpenAPI Specs Download JSON](https://docs.ix-api.net/v1/ix-api-latest.json)
* [OpenAPI Specs Download YAML](https://docs.ix-api.net/v1/ix-api-latest.yml)
* [RFC7807 Problem Details](https://docs.ix-api.net/v1/problems/)

