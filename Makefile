######################################################################
# @author      : Annika Hannig
# @file        : Makefile
# @created     : Thursday Apr 22, 2021 16:44:17 CEST
######################################################################

ODIR := build
SDIR := src

dev:
	cd $(SDIR)/ && hugo serve

production:
	cd $(SDIR)/ && HUGO_ENV=production hugo -d ../$(ODIR)/

.PHONY: clean

clean:
	rm -rf $(ODIR)/

